# riemann-cassandra

A Clojure library designed to provide Riemann and Cassandra integration.

## Usage

### Running Riemann

Add to your Riemann config something like this:

    (ns riemann.config
     (:require [clojurewerkz.cassaforte.client :as client])
     (:use riemann.cassandra.core))

    (def session (client/connect! ["10.0.2.2"]))

    (def cas (cassandra session {}))
    (streams cas)
    (streams #(info "received event" %))

Then run Riemann with this:

    java -cp /usr/lib/riemann/riemann.jar:/vagrant/riemann-cassandra-0.1.0-SNAPSHOT-standalone.jar riemann.bin /etc/riemann/riemann.config

## License

Copyright © 2013 Rafael Durán Castañeda

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
