(ns riemann.cassandra.core
  "Forwards events to Cassandra"
  (:require [clojurewerkz.cassaforte.cql :as cql]))

(defn- insert
  "Insert an event into Cassandra metrics table."
  [cf, event, session]
  (cql/insert cf {:service (:service event)
                  :host (:host event)
                  :time (long (* 1000 (:time event)))
                  :metric (:metric event)}))

(defn cassandra
  "Returns a function to be used with Riemann streams"
  [session, opts]
  (let [opts (merge {:keyspace :metrics
                     :cf :metric} opts)]
    (cql/use-keyspace (:keyspace opts))
    (fn [event]
      (when (:metric event)
        (insert (:cf opts) event session)))))
