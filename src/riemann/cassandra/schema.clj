(ns riemann.cassandra.schema
  (:require [riemann.cassandra.core])
  (:use clojurewerkz.cassaforte.cql
        clojurewerkz.cassaforte.query
        riemann.cassandra.core))

(defn create-schema
  "Create Riemann metrics shcema at Cassandra"
  []
  (create-keyspace :metrics
                   (with {:replication
                          {:class "SimpleStrategy"
                           :replication_factor 1 }}))
  (use-keyspace "metrics")
  (create-table "metric"
                (column-definitions {:service :varchar
                                     :host :varchar
                                     :time :timestamp
                                     :metric :float
                                     :primary-key [:host :service :time]})))
(defn drop-schema
  "Drop Riemann metrics shcema at Cassandra"
  []
  (drop-keyspace "metrics"))
